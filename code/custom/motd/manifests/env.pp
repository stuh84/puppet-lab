class motd::env {
	$pve_host = hiera('pve_host', '')
	file { '/etc/motd':
		content => template('motd/motd.erb'),
		owner   => 'root',
		group   => 'root',
		mode	=> '644',
	}
}
