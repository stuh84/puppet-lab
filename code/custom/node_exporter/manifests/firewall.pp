class node_exporter::firewall {
	if $facts['os']['family'] == 'RedHat' {
		firewalld::custom_service{'node_exporter':
			short       => 'node_exporter',
			description => 'Prometheus Node Expoter',
			port        => [
			  {
			      'port'     => '9100',
			      'protocol' => 'tcp',
			  }
			]
		}
		firewalld_service { 'Allow Node Exporter':
			ensure  => 'present',
			service => 'node_exporter',
			zone    => 'public',
		}
	}
}
