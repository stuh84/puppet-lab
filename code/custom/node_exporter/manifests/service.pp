class node_exporter::service {
	file { '/etc/systemd/system/node_exporter.service':
		content => template('node_exporter/systemd.erb'),
		owner   => 'root',
		group   => 'root',
		mode	=> '644',
	}
	
	service {'node_exporter':
		ensure => 'running',
                enable => true,
	}
}
