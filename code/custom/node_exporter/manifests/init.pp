class node_exporter {
	contain node_exporter::prereqs
	contain node_exporter::install
	contain node_exporter::service
	contain node_exporter::firewall
	contain node_exporter::consul
}
