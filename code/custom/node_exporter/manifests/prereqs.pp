class node_exporter::prereqs {
	group { 'node_exporter':
		name => 'node_exporter',
	}

	user { 'node_exporter':
		name   => 'node_exporter',
		groups => 'node_exporter',
		shell  => '/sbin/nologin',
	}

	file { [ '/opt/node_exporter', '/opt/node_exporter/exporters', '/opt/node_exporter/exporters/dist', '/opt/node_exporter/exporters/dist/textfile' ]:
		ensure => 'directory',
		owner  => 'node_exporter',
		group  => 'node_exporter',
	}
}
