class node_exporter::consul { 
	$consul_service_name = "node_exporter"
	$consul_service_port = 9100

	file { "/etc/consul.d/${consul_service_name}.hcl":
		content => template('consul/consul.prom_service.hcl.erb'),
		owner   => 'root',
		group   => 'root',
		mode	=> '644',
	}	
	
	exec {'consul_reload':
		command => '/usr/bin/consul reload',
		subscribe => [
			File["/etc/consul.d/${consul_service_name}.hcl"],
		],
		refreshonly => true,
        }
}
