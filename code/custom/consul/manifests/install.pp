class consul::install inherits consul {
	group { 'consul':
		name => 'consul',
	}

	user { 'consul':
		name   => 'consul',
		groups => 'consul',
		shell  => '/bin/false',
	}

	file { '/etc/consul.d':
		owner  => 'consul',
		group  => 'consul',
		ensure => 'directory',
	}

	if $facts['os']['family'] == 'Debian' {
		package{'consul': 
			ensure => 'latest',
			require => Class['apt::update']
		}	
	} 
	
	if $facts['os']['family'] == 'RedHat' {
		package{'consul': 
			ensure => 'latest',
		}	
	} 
	
	file { '/var/lib/consul':
		ensure  => 'directory',
		owner   => 'consul',
		group   => 'consul'
	}
}
