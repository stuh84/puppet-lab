class consul::config inherits consul {
	$consul_dc = lookup('consul_vars.dc')
	$consul_enc = lookup('consul_vars.enckey')
	$servers = lookup('consul_vars.servers')
	$is_server = lookup('consul.server')
 
	if $is_server {
		file { '/etc/consul.d/consul.hcl':
			content => template('consul/consul.server.hcl.erb'),
			owner   => 'root',
			group   => 'root',
			mode	=> '644',
		}
	} else {
		file { '/etc/consul.d/consul.hcl':
			content => template('consul/consul.client.hcl.erb'),
			owner   => 'root',
			group   => 'root',
			mode	=> '644',
		}
	}
}
