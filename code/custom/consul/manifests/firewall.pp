class consul::firewall inherits consul {
	if $facts['os']['family'] == 'RedHat' {
		firewalld::custom_service{'consul':
			short       => 'consul',
			description => 'Consul',
			port        => [
			  {
			      'port'     => '8301',
			      'protocol' => 'tcp',
			  },
			  {
			      'port'     => '8500',
			      'protocol' => 'tcp',
			  },
			  {
			      'port'     => '8600',
			      'protocol' => 'tcp',
			  },
			  {
			      'port'     => '8301',
			      'protocol' => 'udp',
			  },
			  {
			      'port'     => '8600',
			      'protocol' => 'udp',
			  },
			]
		}
		firewalld_service { 'Consul':
			ensure  => 'present',
			service => 'consul',
			zone    => 'public',
		}
	}
}
