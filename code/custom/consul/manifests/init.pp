class consul {
        contain hashicorp::repository
	contain consul::install
	contain consul::config
	contain consul::firewall
	contain consul::service

	Class['hashicorp::repository']
	~> Class['consul::install']
	~> Class['consul::config']
	~> Class['consul::firewall']
	~> Class['consul::service']
}
