class consul::service inherits consul {
	service {'consul':
		ensure => 'running',
                enable => true,
		restart => "consul reload",
	}
}
