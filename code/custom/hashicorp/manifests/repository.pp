class hashicorp::repository {
	$distro = $facts['os']['distro']['codename'] 
	if $facts['os']['family'] == 'Debian' {
		apt::source { 'hashicorp':
			comment  => 'Hashicorp Repository',
			location => 'https://apt.releases.hashicorp.com',
			release  => "${distro}",
			repos    => 'main',
			key      => {
				'id'=>'E8A032E094D8EB4EA189D270DA418C88A3219F7B',
				'server' => 'keyserver.ubuntu.com',
			},
			include  => {
				'deb' => true,
			},
		}
	}
	if $facts['os']['family'] ==  'RedHat' {
		yumrepo { 'hashicorp':
			enabled  => 1,
			descr    => 'Hashicorp Repository',
			baseurl  => 'https://rpm.releases.hashicorp.com/RHEL/$releasever/$basearch/stable',
			gpgkey   => 'https://rpm.releases.hashicorp.com/gpg',
			gpgcheck => 1,
			target    => '/etc/yum.repo.d/hashicorp.repo',
  		}	
	}
}
