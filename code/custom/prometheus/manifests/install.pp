class prometheus::install {
	$install_path   = "/usr/local/bin"
        $package_name   = "prometheus"
        $package_type   = "linux-amd64"
        $package_ensure = lookup("prometheus.version") 
        $archive_name   = "${package_name}-${package_ensure}.${package_type}.tar.gz"
        $repository_url = "https://github.com/${package_name}/${package_name}/releases/download"
        $package_source = "${repository_url}/v${package_ensure}/${archive_name}"

	archive { $archive_name:
	  path         => "/tmp/${archive_name}",
	  source       => $package_source,
	  extract      => true,
	  extract_command => 'tar xfz %s --strip-components=1',
	  extract_path => $install_path,
	  creates      => "${install_path}/${package_name}",
	  cleanup      => true,
	}
}	
