class prometheus {
	contain prometheus::prereqs
	contain prometheus::install
	contain prometheus::config
	contain prometheus::service
}
