class prometheus::service {
	file { '/etc/systemd/system/prometheus.service':
		content => template('prometheus/systemd.erb'),
		owner   => 'root',
		group   => 'root',
		mode	=> '644',
	}
	
	service {'prometheus':
		ensure => 'running',
                enable => true,
	}
}
