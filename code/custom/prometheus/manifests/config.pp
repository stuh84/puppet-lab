class prometheus::config {
	$consul_servers = lookup('consul_vars.servers')
	file { '/etc/prometheus/prometheus.yml':
		notify  => Service['prometheus'],
		content => template('prometheus/prometheus.yml.erb'),
		owner   => 'prometheus',
		group   => 'prometheus',
		mode	=> '644',
	}
}
