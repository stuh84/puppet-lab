class prometheus::prereqs {
	group { 'prometheus':
		name => 'prometheus',
	}

	user { 'prometheus':
		name   => 'prometheus',
		groups => 'prometheus',
		shell  => '/sbin/nologin',
	}

	file { [ '/etc/prometheus', '/etc/prometheus/alerts', '/etc/prometheus/rules', '/var/lib/prometheus' ]:
		ensure => 'directory',
		owner  => 'prometheus',
		group  => 'prometheus',
	}
}
